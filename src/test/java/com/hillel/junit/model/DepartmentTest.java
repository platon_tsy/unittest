package com.hillel.junit.model;

import com.hillel.junit.model.staff.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by platon on 20.08.17.
 */
public class DepartmentTest {

    Department itDepartment;
    Department finDepartment;
    int months;

    @Before
    public void setUp() {
        Employee programmer = new Programmer("John", "Johnes", 3000);
        Employee tester = new Tester("Jack", "Daniels", 2800);
        Employee analyst = new Analyst("Sara", "Conor", 2900);

        Employee booker = new Booker("Marya", "Petrovna", 1500);
        Employee finDirector = new FinDirector("Boris", "Petrovich", 4000, 20);

        ArrayList<Employee> itEmployees = new ArrayList<>();
        itEmployees.add(programmer);
        itEmployees.add(tester);
        itEmployees.add(analyst);

        ArrayList<Employee> finEmployees = new ArrayList<>();
        finEmployees.add(booker);
        finEmployees.add(finDirector);

        itDepartment = new Department("IT", itEmployees);
        finDepartment = new Department("FIN", finEmployees);

        ArrayList<Department> departments = new ArrayList<>();
        departments.add(itDepartment);
        departments.add(finDepartment);

        months = 7;
//        Company romashka = new Company("Romashka Company", departments);
//
//        System.out.println(romashka);
//
//
//        romashka.calculateSalary(7);
    }

    @Test
    public void testCalculateSalary() {
        Assert.assertEquals(itDepartment.calculateSalary(months), 60900);
        Assert.assertEquals(finDepartment.calculateSalary(months), 40100);
    }

    @Test
    public void testCalaculateSalaryFor() {

    }

}

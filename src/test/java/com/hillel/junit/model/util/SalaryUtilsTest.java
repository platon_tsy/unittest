package com.hillel.junit.model.util;

import com.hillel.junit.model.staff.Employee;
import com.hillel.junit.model.staff.FinDirector;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by platon on 20.08.17.
 */
public class SalaryUtilsTest {

    FinDirector finDirector;
    int months;

    @Before
    public void setUp() {
        months = 7;
        finDirector = new FinDirector("Boris", "Petrovich", 4000, 20);
    }

    @Test
    public void calculateSalaryFor() {
        int expected = 29600;
        Assert.assertEquals(expected, SalaryUtils.calculateSalaryFor(finDirector, months));
        Assert.assertEquals(8000, SalaryUtils.calculateSalaryFor(finDirector, 2));
    }

}

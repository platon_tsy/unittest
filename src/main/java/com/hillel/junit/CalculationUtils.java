package com.hillel.junit;

/**
 * Created by platon on 20.08.17.
 */
public class CalculationUtils {

    public static int add(int a, int b) {
        return a + b;
    }

    public static int sub(int a, int b) {
        return a - b;
    }
}

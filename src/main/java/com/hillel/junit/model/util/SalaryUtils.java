package com.hillel.junit.model.util;

import com.hillel.junit.model.staff.FinDirector;

/**
 * Created by platon on 20.08.17.
 */
public class SalaryUtils {

    public static int calculateSalaryFor(FinDirector finDirector, int months) {
        int result = 0;
        int countOfBonus = months / 3;
        int bonus = finDirector.getBonus();
        for (int i = 0; i < countOfBonus; i++) {
            int directorsSalary = bonus * finDirector.getSalary() / 100 + finDirector.getSalary();
            result += directorsSalary;
        }
        result += finDirector.getSalary() * (months - countOfBonus);
        return result;
    }

}

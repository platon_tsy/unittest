package com.hillel.junit.model;

import com.hillel.junit.model.staff.Employee;
import com.hillel.junit.model.staff.FinDirector;
import com.hillel.junit.model.util.SalaryUtils;

import java.util.ArrayList;

public class Department {

    private String name;
    private ArrayList<Employee> employees;

    public Department(String name, ArrayList<Employee> employees) {
        this.name = name;
        this.employees = employees;
    }

    public int calculateSalary(int months) {
        int result = 0;
        for (Employee employee : employees) {
            if(employee instanceof FinDirector) {
                result += SalaryUtils.calculateSalaryFor((FinDirector) employee, months);
            } else {
                result += employee.getSalary() * months;
            }
        }
        System.out.println("Department " + this.name + "; salaries = " + result);
        return result;
    }




    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(ArrayList<Employee> employees) {
        this.employees = employees;
    }

    @Override
    public String toString() {
        return "Department{" +
                "name='" + name + '\'' +
                ", employees=" + employees +
                '}';
    }
}

package com.hillel.junit.model.staff;

public class Analyst extends Employee {
    public Analyst(String name, String lastName, int salary) {
        super(name, lastName, salary);
    }
}

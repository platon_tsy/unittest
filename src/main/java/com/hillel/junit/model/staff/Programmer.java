package com.hillel.junit.model.staff;

public class Programmer extends Employee {
    public Programmer(String name, String lastName, int salary) {
        super(name, lastName, salary);
    }
}
